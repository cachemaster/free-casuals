using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace FreeCasuals.Server.Common
{
    public class BaseController : Controller
    {
        protected IActionResult Collection<T>(IReadOnlyCollection<T> collection)
        {
            if (collection == null || collection.Count == 0)
                return NoContent();

            return Ok(collection);
        }
    }
}