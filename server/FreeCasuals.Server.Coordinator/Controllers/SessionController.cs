using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using FreeCasuals.Server.Common;
using FreeCasuals.Server.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace FreeCasuals.Server.Coordinator.Controllers
{
    [Route("api/session")]
    public class SessionController : BaseController
    {
        [HttpGet]
        [AuthorizePlayer]
        public async ValueTask<IActionResult> GetSessions([FromServices] PlayerIdentity playerIdentity, 
            [FromServices] ISessionStorage storage)
        {
            var result = await storage.GetAllSessionsOf(playerIdentity);
            return Collection(result);
        }

        [HttpGet]
        [AuthorizePlayer]
        public async ValueTask<IActionResult> GetActiveSessions([FromServices] PlayerIdentity playerIdentity,
            [FromServices] ISessionStorage storage)
        {
            var result = (await storage.GetAllSessionsOf(playerIdentity))
                .Where(s => s.IsActive)
                .ToImmutableArray();

            return Collection(result);
        }
        
        [HttpGet]
        [AuthorizePlayer]
        public async ValueTask<IActionResult> GetOldSessions([FromServices] PlayerIdentity? playerIdentity,
            [FromServices] ISessionStorage storage)
        {
            var result = (await storage.GetAllSessionsOf(playerIdentity))
                .Where(s => !s.IsActive)
                .ToImmutableArray();

            return Collection(result);
        }
        
        
    }

    public static class ControllerExtensions
    {
        public static IActionResult Collection()    
    }
    
    internal class AuthorizePlayerAttribute : ActionFilterAttribute
    {
        private const string PlayerIdentityArguement = "playerIdentity";
        
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ActionArguments.ContainsKey("")
        }
    }

    public interface ISessionStorage
    {
        ValueTask<IReadOnlyList<Session>> GetAllSessionsOf(PlayerIdentity? playerIdentity);
    }
}