namespace FreeCasuals.Server.Contracts
{
    public struct Session
    {
        public bool IsActive { get; set; }
    }
}